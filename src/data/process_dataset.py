import pandas as pd


df = pd.read_csv("data/raw/powerconsumption.csv")

datetime = pd.to_datetime(df["Datetime"])


def datetime_to_minutes(dt):
    return dt.time().hour * 60 + dt.time().minute


df = pd.DataFrame({
    "Datetime": datetime,
    "Minutes": datetime.apply(datetime_to_minutes),
    "Temperature": df["Temperature"],
    "Humidity": df["Humidity"],
    "WindSpeed": df["WindSpeed"],
    "GeneralDiffuseFlows": df["GeneralDiffuseFlows"],
    "DiffuseFlows": df["DiffuseFlows"],
    "PowerConsumption_Zone1": df["PowerConsumption_Zone1"],
    "PowerConsumption_Zone2": df["PowerConsumption_Zone2"],
    "PowerConsumption_Zone3": df["PowerConsumption_Zone3"],
})

labels = [
    "Minutes",
    "Temperature",
    "Humidity",
    "WindSpeed",
    "GeneralDiffuseFlows",
    "DiffuseFlows",
    "PowerConsumption_Zone1",
    "PowerConsumption_Zone2",
    "PowerConsumption_Zone3",
]
df[labels] = (df[labels] - df[labels].min()) / \
    (df[labels].max() - df[labels].min())

df.to_csv("data/processed/powerconsumption.csv", index=False)
