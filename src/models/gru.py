import torch.nn as nn


class GRUModel(nn.Module):
    def __init__(self, look_back_hours: int, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)

        assert look_back_hours > 0
        back_sz = 6 * look_back_hours

        self.gru = nn.GRU(back_sz, 1, batch_first=True)
        self.flatten = nn.Flatten()
        self.fc = nn.Linear(7, 1)

    def forward(self, x):
        y, _ = self.gru(x)
        y = self.flatten(y)
        y = self.fc(y)
        return y
