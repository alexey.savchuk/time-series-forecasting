from typing import Literal

import pandas as pd
import torch

from torch.utils.data import Dataset


class PowerConsumptionDataset(Dataset):
    def __init__(
        self,
        look_back_hours: int,
        look_ahead_hours: int,
        zone: Literal[1, 2, 3],
        *args,
        **kwargs
    ) -> None:
        super().__init__(*args, **kwargs)

        assert look_back_hours > 0
        assert look_ahead_hours > 0
        assert zone in (1, 2, 3)

        back_sz = 6 * look_back_hours
        ahead_sz = 6 * look_ahead_hours

        self.frame = pd.read_csv("../data/processed/powerconsumption.csv")
        self.frame["Datetime"] = pd.to_datetime(self.frame["Datetime"])

        zone_label = f"PowerConsumption_Zone{zone}"
        self.X_frame = self.frame[
            [
                "Minutes", "Temperature", "Humidity", "WindSpeed",
                "GeneralDiffuseFlows", "DiffuseFlows", zone_label
            ]
        ]
        self.y_frame = self.frame[zone_label]

        if back_sz + ahead_sz + 1 > len(self.frame):
            raise RuntimeError("window size exceeds dataset size")

        self.back_sz = back_sz
        self.ahead_sz = ahead_sz

    def __len__(self) -> int:
        return len(self.frame) - self.back_sz - self.ahead_sz

    def __getitem__(self, idx):
        if idx < 0 or idx >= len(self):
            raise IndexError("index out of range")

        xi = self.X_frame.iloc[idx:idx + self.back_sz]
        yi = self.y_frame.iloc[idx + self.back_sz + self.ahead_sz]

        xi = torch.tensor(xi.values, dtype=torch.float32).transpose(0, 1)
        yi = torch.tensor(yi, dtype=torch.float32)

        return xi, yi
